﻿namespace WorkerSystem
{
	public interface IWorker
	{
		#region PublicMethods

		void Execute();

		#endregion
	}
}
