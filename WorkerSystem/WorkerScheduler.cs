﻿using System;
using System.Collections.Generic;
using System.Security;

namespace WorkerSystem
{
	public sealed class WorkerScheduler<TWorker, TPayload> : IDisposable
		where TWorker : WorkerWrapper<TPayload>, new()
		where TPayload : WorkerPayload
	{
		#region PublicFields

		public bool IsInitialized => !m_disposed;

		#endregion

		#region PrivateFields

		private List<TWorker> m_workers = new List<TWorker>();
		private List<WorkerHandle> m_handles = new List<WorkerHandle>();
		private bool m_disposed;

		#endregion

		#region PublicMethods

		/// <summary>
		///     Dispose all resources and abort all workers
		/// </summary>
		public void Dispose()
		{
			if (m_disposed)
			{
				return;
			}

			m_workers.Clear();
			m_handles.Clear();

			m_disposed = true;
		}

		public void Initialize(int initCount = 10)
		{
			for (var i = 0; i < initCount; i++)
			{
				GetWorker();
			}
		}

		/// <summary>
		///     Wait for task completion. Pauses current thread until the task has completed all payloads.
		/// </summary>
		public void CompleteAll()
		{
			if (m_handles.Count <= 0)
			{
				return;
			}

			m_handles.RemoveAll(item => item.IsCompleted);

			foreach (WorkerHandle worker in m_handles)
			{
				worker.Complete();
			}
		}

		/// <summary>
		///     Triggers execution once on an empty payload.
		/// </summary>
		[SecurityCritical]
		public void Run()
		{
			Schedule(null);
		}

		/// <summary>
		///     Schedules new payload for execution on free worker.
		/// </summary>
		[SecurityCritical]
		public TWorker Schedule(TPayload payload)
		{
			TWorker worker = GetWorker();
			worker.SetPayload(payload);
			m_handles.Add(worker.Schedule());

			return worker;
		}

		#endregion

		#region PrivateMethods

		private TWorker GetWorker()
		{
			foreach (TWorker worker in m_workers)
			{
				if (worker.IsFree)
				{
					return worker;
				}
			}

			var newWorker = new TWorker();
			m_workers.Add(newWorker);
			return newWorker;
		}

		#endregion

		#region AllOtherMembers

		~WorkerScheduler()
		{
			Dispose();
		}

		#endregion
	}
}
