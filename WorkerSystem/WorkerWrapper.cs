﻿namespace WorkerSystem
{
	public abstract class WorkerWrapper<TPayload> : IWorker
		where TPayload : WorkerPayload
	{
		#region PublicFields

		public bool IsFree { get; private set; }

		#endregion

		#region PrivateFields

		private TPayload m_payload;

		#endregion

		#region PublicMethods

		public void Execute()
		{
			IsFree = false;

			if (m_payload == null)
			{
				ImplRun();
			}
			else
			{
				ImplExecute(m_payload);
			}

			Cleanup();
			m_payload = null;
			IsFree = true;
		}

		public void SetPayload(TPayload payload)
		{
			IsFree = false;
			m_payload = payload;
		}

		public abstract void Cleanup();

		#endregion

		#region ProtectedMethods

		protected abstract void ImplExecute(TPayload payload);
		protected abstract void ImplRun();

		#endregion
	}
}
