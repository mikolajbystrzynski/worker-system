﻿namespace WorkerSystem
{
	public class WorkerHandle
	{
		#region PublicFields

		public volatile bool IsCompleted;
		public IWorker Worker;

		#endregion

		#region OtherFields

		internal PersistentWorker Thread;

		#endregion

		#region Constructors

		internal WorkerHandle(IWorker worker, PersistentWorker thread)
		{
			IsCompleted = false;
			Worker = worker;
			Thread = thread;
		}

		#endregion
	}
}
