﻿using System.Collections.Generic;
using System.Threading;

namespace WorkerSystem
{
	internal sealed class PersistentWorker
	{
		#region PublicFields

		public Thread CurrentThread => Thread.CurrentThread;

		public volatile bool Idle = true;
		public AutoResetEvent ResetEvent = new AutoResetEvent(false);

		public volatile Queue<WorkerHandle> Works = new Queue<WorkerHandle>();

		#endregion

		#region PrivateFields

		private volatile bool m_abort;

		#endregion

		#region PublicMethods

		public void Abort()
		{
			foreach (WorkerHandle handle in Works)
			{
				handle.IsCompleted = true;
			}

			Works.Clear();
			m_abort = true;

			// Only now reset execution
			ResetEvent.Set();
		}

		public void Enqueue(WorkerHandle worker)
		{
			if (Works == null)
			{
				Works = new Queue<WorkerHandle>();
			}

			Idle = false;
			lock (Works)
			{
				Works.Enqueue(worker);
			}

			// Only now reset execution
			ResetEvent.Set();
		}

		public void Execute(object param)
		{
			while (!m_abort)
			{
				if (Works.Count <= 0)
				{
					Idle = true;
					ResetEvent.WaitOne();
					continue;
				}

				Idle = false;

				WorkerHandle handle = null;

				try
				{
					lock (Works)
					{
						handle = Works.Dequeue();
					}

					if (handle?.Worker != null && !handle.IsCompleted)
					{
						handle.Worker.Execute();
					}
				}
				catch
				{
					throw;
				}
				finally
				{
					if (handle != null)
					{
						handle.IsCompleted = true;
					}
				}
			}
		}

		#endregion
	}
}
