﻿using System.Collections.Generic;
using System.Threading;

namespace WorkerSystem
{
	public static class WorkerPool
	{
		#region PrivateFields

		private const string THREAD_NAME = "WorkerThread";

		private static bool isInitialized;

		private static int allowedSystemThreadCount;
		private static Dictionary<Thread, PersistentWorker> workerThreads;

		#endregion

		#region PublicMethods

		public static void Complete(this WorkerHandle handle)
		{
			var count = 1000;

			while (count > 0)
			{
				if (handle.IsCompleted)
				{
					return;
				}

				count++;
			}

			throw new ThreadStateException
			(
				"Thread timeout\n" +
				$"Worker Name: {handle.Worker.GetType().Name}\n" +
				$"Thread ID: {handle.Thread.CurrentThread.ManagedThreadId}\n" +
				$"Works count: {handle.Thread.Works.Count}\n" +
				$"Contains handle: {handle.Thread.Works.Contains(handle)}\n" +
				$"Idle: {handle.Thread.Idle}");
		}

		public static WorkerHandle Schedule(this IWorker worker)
		{
			ConditionalInit();

			PersistentWorker workerLow = FindLowestLoadWorker();
			var handle = new WorkerHandle(worker, workerLow);
			workerLow.Enqueue(handle);

			return handle;
		}

		public static WorkerHandle Schedule(this IWorker worker, WorkerHandle dependency)
		{
			ConditionalInit();

			PersistentWorker workerThread = dependency.Thread;
			var handle = new WorkerHandle(worker, workerThread);
			workerThread.Enqueue(handle);

			return handle;
		}

		public static void Initialize()
		{
			ConditionalInit();
		}

		public static void Shutdown()
		{
			if (!isInitialized)
			{
				return;
			}

			if (workerThreads != null)
			{
				foreach (var kvp in workerThreads)
				{
					kvp.Value.Abort();
				}

				workerThreads = null;
			}

			allowedSystemThreadCount = 0;

			isInitialized = false;
		}

		#endregion

		#region PrivateMethods

		private static PersistentWorker FindLowestLoadWorker()
		{
			PersistentWorker tmp = null;
			int lowestSoFar = int.MaxValue;

			foreach (var kvp in workerThreads)
			{
				if (kvp.Value.Works.Count < lowestSoFar)
				{
					lowestSoFar = kvp.Value.Works.Count;
					tmp = kvp.Value;
				}
			}

			return tmp;
		}

		private static void ConditionalInit()
		{
			if (isInitialized)
			{
				return;
			}

			ThreadPool.GetMinThreads(out allowedSystemThreadCount, out int ignore);

			int max = allowedSystemThreadCount - 1;
			workerThreads = new Dictionary<Thread, PersistentWorker>(max);

			for (var i = 0; i < max; i++)
			{
				var workerThread = new PersistentWorker();

				var thread = new Thread(workerThread.Execute);
				thread.SetApartmentState(ApartmentState.MTA);
				thread.IsBackground = true;
				thread.Name = THREAD_NAME;
				thread.Priority = ThreadPriority.Normal;

				workerThreads.Add(thread, workerThread);
				thread.Start();
			}

			isInitialized = true;
		}

		#endregion
	}
}
